package com.example.tp3;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptateur extends ArrayAdapter<CityActivity>{
    private static final Object City = 0 ;
    Context context;
    ArrayList<City> cities;
    private LayoutInflater inflate;

    public Adaptateur(Context context, ArrayList<City> cities) {
        super(context, 0, (CityActivity[]) City);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
     //   City city = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_city, parent, false);
        }
        // Lookup view for data population
        TextView  name = (TextView) convertView.findViewById(R.id.nameCity);
        TextView country = (TextView) convertView.findViewById(R.id.country);
        TextView temperature =(TextView) convertView.findViewById(R.id.temperature);
        TextView humidity = (TextView) convertView.findViewById(R.id.editHumidity);
        // Populate the data into the template view using the data object

        /*
        name.setText(name.getnamecity());
        country.setText(country.getcountry());
        temperature.setText(temperature.getTemperature());
        humidity.setText(humidity.getHumidity());
        windSpeed.setText(windSpeed.getWindSpeed());

        auteur.setText(book.getAuthors());
        // Return the completed view to render on screen

        */
        return convertView;
    }

}
