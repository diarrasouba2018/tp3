package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.net.URL;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    WeatherDbHelper dbHelper;
    SimpleCursorAdapter cursorAdapter;
    Cursor cityCursor;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       // Adaptateur adapt = new Adaptateur(this,CityListe.getALLCitys());

        listView = findViewById(R.id.listview);
        registerForContextMenu(listView);

        //swipeRefreshLayout  = findViewById(R.id.swipeRefreshLayout);
/*        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MyAsyncTask asyncTask = new MyAsyncTask () ;
                asyncTask.onPreExecute();
            }
        });
*/

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,NewCityActivity.class);
                startActivityForResult(intent,1);
            }

        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                City city = WeatherDbHelper.cursorToCity(cursor);

                Intent intent = new Intent(MainActivity.this,CityActivity.class);
                intent.putExtra(City.TAG,city);
                startActivityForResult(intent,1);
            }
        });

        populateListView();
    }
    //a revoir
    private void populateListView() {
        dbHelper = new WeatherDbHelper(getApplicationContext());
        dbHelper.populate();
        cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2, dbHelper.fetchAllCities () ,
                new String[] { WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY },  new int[] { android.R.id.text1, android.R.id.text2});
        listView.setAdapter(cursorAdapter);
        cursorAdapter.notifyDataSetChanged();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        City citis = data.getExtras().getParcelable(City.TAG);

        if(resultCode==1){dbHelper.addCity(citis);}
        else if(requestCode==2){dbHelper.updateCity(citis);}
        populateListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
  /*  @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId())
        {
           case R.id.deleteItem:
                Cursor cusor = (Cursor) listView.getItemAtPosition(info.position);
                dbHelper.deleteCity(cusor);
                populateListView();
                return true;
            default :
                return super.onContextItemSelected(item);
       }

    }*/

    private class Task extends AsyncTask<String, String, String> {

        private String resp=null;

        JSONResponseHandler jsonResponse;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping...");
            try {
                cityCursor = dbHelper.fetchAllCities();
                City city;
                try {
                    cityCursor.moveToPosition(-1);
                    while (cityCursor.moveToNext()) {
                        city=WeatherDbHelper.cursorToCity(cityCursor);
                        URL url = WebServiceUrl.build(city.getName(),city.getCountry());
                        jsonResponse = new JSONResponseHandler(city);
                        try {
                            jsonResponse.readJsonStream(url.openStream());
                        }catch (Exception e){

                        }

                        dbHelper.updateCity(city);
                    }
                } finally {
                    cityCursor.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String result) {
            swipeRefreshLayout.setRefreshing(false);
            populateListView();

        }

        @Override
        protected void onProgressUpdate(String... text) {

        }

    }
}