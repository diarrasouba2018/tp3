package com.example.tp3;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;

public class CityActivity extends AppCompatActivity {
    WeatherDbHelper dbHelper;

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        if(Build.VERSION.SDK_INT >9)
        {
            StrictMode.ThreadPolicy policy= new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        dbHelper = new WeatherDbHelper(this);

        city = (City) getIntent().getParcelableExtra(City.TAG);

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);
        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                Task asyncTask = new Task();
                asyncTask.execute();




            }
        });

    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        Intent intent;
        intent = new Intent();
        intent.putExtra(city.TAG, city);
        setResult(2, intent);
        super.onBackPressed();
    }

    private void updateView() {
        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature()+" °C");
        textHumdity.setText(city.getHumidity()+" %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity()+" %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            Log.d(TAG,"icon="+"icon_" + city.getIcon());
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }

    }



    private class Task extends AsyncTask<String, String, String> {

        private String resp=null;
        ProgressDialog progressDialog;
        JSONResponseHandler jsonResponse;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                URL url = WebServiceUrl.build(city.getName(),city.getCountry());
                jsonResponse = new JSONResponseHandler(city);
                jsonResponse.readJsonStream(url.openStream());

                //Thread.sleep(5000);
            } catch (Exception e) {
                resp = e.getMessage();
                e.printStackTrace();
            }
            return resp;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(CityActivity.this,
                    "Mise à jour",
                    "Veuillez patienter un instant ...");
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            if(resp.isEmpty()){
            city=jsonResponse.getCity;
            updateView();

          }else{
                Toast.makeText(getApplicationContext(),"Verifiez votre connexion internet",Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onProgressUpdate(String... text) {

        }
    }




}